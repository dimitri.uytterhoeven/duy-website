import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileModule } from './modules/profile/profile.module';
import { AsideModule } from './modules/aside/aside.module';
import { MenuModule } from './modules/menu/menu.module';
import { ParticlesModule } from 'angular-particle';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    ProfileModule,
    AsideModule,
    MenuModule,
    ParticlesModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar: true,
      tapToDismiss: true,
      newestOnTop: true,
      maxOpened: 5,
      timeOut: 3000,
      positionClass: 'toast-top-center',
      preventDuplicates: false,
    }) // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
