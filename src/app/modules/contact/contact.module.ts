import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  ErrorStateMatcher,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    ContactRoutingModule,
    FontAwesomeModule,
    NgSlimScrollModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule

  ],
  providers: [{
    provide: SLIMSCROLL_DEFAULTS,
    useValue: {
      alwaysVisible: false,
      gridOpacity: '0.2', barOpacity: '0.5',
      gridBackground: '#fff',
      gridWidth: '6',
      gridMargin: '2px 2px',
      barBackground: '#fff',
      barWidth: '20',
      barMargin: '2px 2px'
    }},
    {
      provide: ErrorStateMatcher,
      useClass: ShowOnDirtyErrorStateMatcher
    }
  ],
})
export class ContactModule { }
