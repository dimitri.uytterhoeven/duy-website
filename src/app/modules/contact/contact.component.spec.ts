import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactComponent } from './contact.component';
import { CommonModule } from '@angular/common';
import { ContactRoutingModule } from './contact-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ToastrConfig, ToastrModule, ToastrService } from 'ngx-toastr';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactComponent ],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        CommonModule,
        ContactRoutingModule,
        FontAwesomeModule,
        NgSlimScrollModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        ToastrModule.forRoot({
          progressBar: true,
          tapToDismiss: true,
          newestOnTop: true,
          maxOpened: 5,
          timeOut: 3000,
          positionClass: 'toast-top-center',
          preventDuplicates: false,
        })
      ],
      providers: [ HttpClient, HttpHandler, ToastrService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
