import { Component, EventEmitter, HostBinding, OnInit } from '@angular/core';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import {
  faMapMarkerAlt,
  faPaperPlane,
  faMobileAlt,
  faBriefcase,
  faEnvelope,
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SendEmailService } from '../../core/services/send-email.service';
import { Contact } from '../../core/models/Contact';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  faTimes = faTimes;

  contact: Contact;

  contactFormControl: FormGroup;
  submitted = false;
  displayCancelButton = false;

  matcher = new MyErrorStateMatcher();

  faPaperPlane = faPaperPlane;
  faMapMarkerAlt = faMapMarkerAlt;
  faMobileAlt = faMobileAlt;
  faBriefcase = faBriefcase;
  faEnvelope = faEnvelope;

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  // it was in order to be able to apply style on router children
  @HostBinding('class') class = 'full-space-container';

  constructor(private formBuilder: FormBuilder,
              private sendEmailService: SendEmailService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.contactFormControl = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.maxLength(256)]]
    });

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right', // left | right
      barBackground: 'grey', // #C9C9C9
      barOpacity: '0.6', // 0.8
      barWidth: '6', // 10
      barBorderRadius: '70', // 20
      barMargin: '0', // 0
      gridBackground: '#d9d9d9', // #D9D9D9
      gridOpacity: '0', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible: false, // true
      visibleTimeout: 1000, // 1000
    };

    this.checkFormChanges();
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactFormControl.controls; }

  onSubmit() {
    this.submitted = true;
    this.contactFormControl.disable();

    // stop here if form is invalid
    if (this.contactFormControl.invalid) {
      this.toastr.error('Ow snap, your form is invalid ! :(');
      return;
    }
    console.log('message sent for:', this.contact);
    this.sendEmailService.sendEmail(this.contact).subscribe((response) => {
      console.log('message sent for:', this.contact);
      console.log(response);
    }, () => {
      this.toastr.error('Something went wrong during the sending of the message!');
      this.contactFormControl.enable();
    }, () => {
      this.toastr.success('Message sent successfully!');
      this.contactFormControl.enable();
    });
  }

  onReset() {
    this.submitted = false;
    this.contactFormControl.reset();
  }

  checkFormChanges() {
    let cancelButtonShouldBeDisplayed = false;
    this.contactFormControl.valueChanges
        .pipe(
          map((value) => {
            cancelButtonShouldBeDisplayed = false;
            for (const key in value) {
              if (value[key]) {
                cancelButtonShouldBeDisplayed =  true;
              }
            }
            this.displayCancelButton = cancelButtonShouldBeDisplayed;
        })).subscribe();
  }

}
