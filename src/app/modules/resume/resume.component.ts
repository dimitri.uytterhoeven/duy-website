import { Component, EventEmitter, HostBinding, OnInit } from '@angular/core';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { faBriefcase, faGraduationCap } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss'],
})
export class ResumeComponent implements OnInit {

  faBriefcase = faBriefcase;
  faGraduationcap = faGraduationCap;
  // TODO add some images and videos in order to make it nicer

  skillsContentSmall = false;

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  experiences = [
    {
      title: 'Senior IT Consultant',
      subtitle: '@ Sopra Banking Software',
      date: '2016-present',
      description: '2.5 years of Backend and scripting<br>' +
                   '1.5 years of Frontend and automation<br><br>' +
                   'Intrapreneuship in Digital Banking Innovation',
      icon: faBriefcase
    },
    {
      title: 'Electromechanical Engineer',
      subtitle: '@ Université Catholique de Louvain',
      date: '2010-2015',
      description: 'Thesis: "Movement of a humanoid robot on two wheels : NAO’Bike"',
      icon: faGraduationCap
    },
  ];

  skills = [
    {category: 'Git', filled: Array(5).fill(0), empties: Array(0).fill(0)},
    {category: 'Angular 2+', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Reactjs', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Java', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Jmeter', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Postman', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Gitlab CI/CD', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Docker', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'HTML5', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Firebase', filled: Array(3).fill(0), empties: Array(2).fill(0)},
    {category: 'CSS3', filled: Array(4).fill(0), empties: Array(1).fill(0)},
    {category: 'Javascript', filled: Array(3).fill(0), empties: Array(2).fill(0)},
    {category: 'Python', filled: Array(3).fill(0), empties: Array(2).fill(0)},
    {category: 'NodeJs', filled: Array(3).fill(0), empties: Array(2).fill(0)},
    {category: 'Vuejs', filled: Array(2).fill(0), empties: Array(3).fill(0)},
    {category: 'Spring', filled: Array(2).fill(0), empties: Array(3).fill(0)},
    {category: 'Microservices', filled: Array(2).fill(0), empties: Array(3).fill(0)},
    {category: 'Jenkins', filled: Array(2).fill(0), empties: Array(3).fill(0)},
    {category: 'Redux', filled: Array(1).fill(0), empties: Array(4).fill(0)},
    {category: 'Pen-testing', filled: Array(1).fill(0), empties: Array(4).fill(0)},
  ];

  // it was in order to be able to apply style on router children
  @HostBinding('class') class = 'full-space-container';

  constructor() { }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right', // left | right
      barBackground: 'grey', // #C9C9C9
      barOpacity: '0.6', // 0.8
      barWidth: '6', // 10
      barBorderRadius: '70', // 20
      barMargin: '0', // 0
      gridBackground: '#d9d9d9', // #D9D9D9
      gridOpacity: '0', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible: false, // true
      visibleTimeout: 1000, // 1000
    };
  }

}
