import { trigger, state, style, animate, transition, query, animateChild, group } from '@angular/animations';

// FIXME: solve this animation
// this animation is not really used since nor working as expected
// the goal was to animate the menu rendering on mobile to make it slide
// and fade in from the left/ Should be perhaps an issue with
// display: block and display:none attributes and animations
export const fadeInAnimation =
  trigger('fadeIn', [
    transition(':enter', [
      style({ left: '-100px', opacity: '0'}),
      animate('1s ease-out', style({ left: '0px', opacity: '1'})),
    ]),
    transition(':leave', [
      style({ left: '0px', opacity: '1' }),
      animate('1s ease-out', style({ opacity: '0'})),
    ]),
  ]);
