import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { faBars, faIdCard, faTimes } from '@fortawesome/free-solid-svg-icons';
import { fadeInAnimation } from './menu-animation';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    fadeInAnimation,
  ],
})
export class MenuComponent implements OnInit {

  faIdCard = faIdCard;
  faBars = faBars;
  faTimes = faTimes;

  menuDisplay: boolean;
  selected: string;

  items = [
    {name: 'PROFILE', icon: faIdCard, link: 'profile', library: 'font-awesome', canHide: true},
    {name: 'ABOUT', icon: 'person', link: 'about', library: 'material', canHide: false},
    {name: 'RESUME', icon: 'library_books', link: 'resume', library: 'material', canHide: false},
    {name: 'PORTFOLIO', icon: 'brush', link: 'portfolio', library: 'material', canHide: false},
    {name: 'CONTACT', icon: 'alternate_email', link: 'contact', library: 'material', canHide: false}
  ];

  // in mobile, if you click outside the menu, it will close it
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.hideMenu();
    }
  }

  constructor(private router: Router, private eRef: ElementRef) {
    this.menuDisplay = false;
  }

  ngOnInit() {
    this.selected = this.items[0].link;
    if (window.outerWidth > 420) {
      this.router.navigate([`/aside/${this.items[1].link}`]);
      this.selected = this.items[1].link;
    }
  }

  select(item) {
    this.selected = item.link;
  }

  isActive(item) {
    return this.selected === item.link;
  }

  onResize(event) {
    if (event.target.innerWidth > 750 && this.selected === 'profile') {
      this.select(this.items[1]);
      this.router.navigate([`/aside/${this.items[1].link}`]);
    } // window width
  }

  toggleMenuDisplay() {
    this.menuDisplay = !this.menuDisplay;
  }

  hideMenu() {
    this.menuDisplay = false;
  }

}
