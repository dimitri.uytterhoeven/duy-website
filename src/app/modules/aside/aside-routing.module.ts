import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from '../about/about.component';
import { PortfolioComponent } from '../portfolio/portfolio.component';
import { ResumeComponent } from '../resume/resume.component';
import { ContactComponent } from '../contact/contact.component';
import { ProfileComponent } from '../profile/profile.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/aside/profile',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'aside/profile',
        component: ProfileComponent,
        data: {animation: 'ProfilePage'}
      },
      {
        path: 'aside/about',
        component: AboutComponent,
        data: {animation: 'AboutPage'}
      },
      {
        path: 'aside/portfolio',
        component: PortfolioComponent,
        data: {animation: 'PortfolioPage'}
      },
      {
        path: 'aside/resume',
        component: ResumeComponent,
        data: {animation: 'ResumePage'}
      },
      {
        path: 'aside/contact',
        component: ContactComponent,
        data: {animation: 'ContactPage'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsideRoutingModule { }
