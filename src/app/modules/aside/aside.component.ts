import { Component, OnInit } from '@angular/core';
import { slideInAnimation } from './aside-animation';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class AsideComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  prepareRoute(outlet: RouterOutlet) {
    const attribute = 'animation';
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData[attribute];
  }
}
