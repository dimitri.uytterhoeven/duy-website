import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsideComponent } from './aside.component';
import { CommonModule } from '@angular/common';
import { AsideRoutingModule } from './aside-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutModule } from '../about/about.module';
import { PortfolioModule } from '../portfolio/portfolio.module';
import { ResumeModule } from '../resume/resume.module';
import { ContactModule } from '../contact/contact.module';
import { ProfileModule } from '../profile/profile.module';
import { ChildrenOutletContexts, RouterOutlet } from '@angular/router';

describe('AsideComponent', () => {
  let component: AsideComponent;
  let fixture: ComponentFixture<AsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsideComponent ],
      imports: [
        CommonModule,
        AsideRoutingModule,
        BrowserAnimationsModule,
        AboutModule,
        PortfolioModule,
        ResumeModule,
        ContactModule,
        ProfileModule
      ],
      providers: [ RouterOutlet, ChildrenOutletContexts ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
