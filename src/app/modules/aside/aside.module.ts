import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsideRoutingModule } from './aside-routing.module';
import { AsideComponent } from './aside.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutModule} from '../about/about.module';
import { PortfolioModule } from '../portfolio/portfolio.module';
import { ContactModule } from '../contact/contact.module';
import { ResumeModule } from '../resume/resume.module';


@NgModule({
  declarations: [AsideComponent],
  exports: [
    AsideComponent
  ],
  imports: [
    CommonModule,
    AsideRoutingModule,
    BrowserAnimationsModule,
    AboutModule,
    PortfolioModule,
    ResumeModule,
    ContactModule
  ]
})
export class AsideModule { }
