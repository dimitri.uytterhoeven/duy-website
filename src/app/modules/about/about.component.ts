import { Component, EventEmitter, HostBinding, OnInit } from '@angular/core';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { faHandshake, faLaptopCode, faLayerGroup, faUniversity } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {

  faUniversity = faUniversity;
  faLaptopCode = faLaptopCode;
  faLayerGroup = faLayerGroup;
  faHandshake = faHandshake;

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  // it was in order to be able to apply style on router children
  @HostBinding('class') class = 'full-space-container';

  constructor() { }

  ngOnInit() {

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right', // left | right
      barBackground: 'grey', // #C9C9C9
      barOpacity: '0.6', // 0.8
      barWidth: '6', // 10
      barBorderRadius: '70', // 20
      barMargin: '0', // 0
      gridBackground: '#d9d9d9', // #D9D9D9
      gridOpacity: '0', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible: false, // true
      visibleTimeout: 1000, // 1000
    };
  }

}
