import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutComponent } from './about.component';
import { CommonModule } from '@angular/common';
import { AboutRoutingModule } from './about-routing.module';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AboutRoutingModule,
        NgSlimScrollModule,
        FontAwesomeModule,
      ],
      declarations: [ AboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
