import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSocialComponent } from './profile-social.component';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfileRoutingModule } from '../../profile-routing.module';
import { FileDownloadService } from '../../../../core/services/file-download.service';

describe('SocialComponent', () => {
  let component: ProfileSocialComponent;
  let fixture: ComponentFixture<ProfileSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FontAwesomeModule,
        ProfileRoutingModule
      ],
      declarations: [ ProfileSocialComponent ],
      providers: [ FileDownloadService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
