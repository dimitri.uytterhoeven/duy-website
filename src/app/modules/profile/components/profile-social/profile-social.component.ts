import { Component, OnInit } from '@angular/core';
import {faTwitter, faDribbble, faGithub, faStackOverflow, faLinkedin} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-profile-social',
  templateUrl: './profile-social.component.html',
  styleUrls: ['./profile-social.component.scss']
})
export class ProfileSocialComponent implements OnInit {
  faTwitter = faTwitter;
  faDribbble = faDribbble;
  faGithub = faGithub;
  faStackOverflow = faStackOverflow;

  icons = [
    {name: 'twitter', icon: faTwitter, url: 'https://twitter.com/UytterhoevenD'},
    {name: 'dribbble', icon: faDribbble,  url: 'https://dribbble.com/duytterhoeven'},
    {name: 'github', icon: faGithub,  url: 'https://github.com/eldala07'},
    {name: 'stackOverflow', icon: faStackOverflow,  url: 'https://stackoverflow.com/users/10138943/eldala07'},
    {name: 'linkedin', icon: faLinkedin,  url: 'https://www.linkedin.com/in/dimitri-uytterhoeven-a4071761/'}
  ];

  constructor() { }

  ngOnInit() {
  }

  redirectSocial(item) {
    console.log(`redirectSocial hit ${item.name}`);
  }

}
