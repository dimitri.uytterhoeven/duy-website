import { Component, OnInit } from '@angular/core';
import { FileDownloadService } from 'src/app/core/services/file-download.service';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-profile-actions',
  templateUrl: './profile-actions.component.html',
  styleUrls: ['./profile-actions.component.scss']
})
export class ProfileActionsComponent implements OnInit {

  // Variable used to stop the blinking animation
  over = true;

  constructor(private fileDownloadService: FileDownloadService) { }

  ngOnInit() {
  }

  downloadResume() {
    this.fileDownloadService.downloadFile('/assets/documents/resume/duy.pdf').subscribe((blob) => {
      const url = window.URL.createObjectURL(blob);
      window.open(url);
      fileSaver.saveAs(blob, 'CV_DIMITRI_UYTTERHOEVEN.pdf');
    }, (error) => { console.log('Error downloading the file', error); },
        () => { console.log('File downloaded successfully'); }
    );
  }

}
