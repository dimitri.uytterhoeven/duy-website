import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileActionsComponent } from './profile-actions.component';
import { FileDownloadService } from '../../../../core/services/file-download.service';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfileRoutingModule } from '../../profile-routing.module';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('ActionsComponent', () => {
  let component: ProfileActionsComponent;
  let fixture: ComponentFixture<ProfileActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FontAwesomeModule,
        ProfileRoutingModule
      ],
      declarations: [ ProfileActionsComponent ],
      providers: [ FileDownloadService, HttpClient, HttpHandler ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
