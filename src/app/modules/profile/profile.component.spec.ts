import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { ProfileSocialComponent } from './components/profile-social/profile-social.component';
import { ProfileActionsComponent } from './components/profile-actions/profile-actions.component';
import { FileDownloadService } from '../../core/services/file-download.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FontAwesomeModule,
      ],
      declarations: [ ProfileComponent, ProfileSocialComponent, ProfileActionsComponent ],
      providers: [ FileDownloadService, HttpClient, HttpHandler ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
