import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileSocialComponent } from './components/profile-social/profile-social.component';
import { ProfileActionsComponent } from './components/profile-actions/profile-actions.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [ProfileComponent, ProfileSocialComponent, ProfileActionsComponent],
  exports: [
    ProfileComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
