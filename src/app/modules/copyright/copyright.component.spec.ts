import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyrightComponent } from './copyright.component';
import { CommonModule } from '@angular/common';
import { CopyrightRoutingModule } from './copyright-routing.module';

describe('CopyrightComponent', () => {
  let component: CopyrightComponent;
  let fixture: ComponentFixture<CopyrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
      CommonModule,
      CopyrightRoutingModule,
      ],
      declarations: [ CopyrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
