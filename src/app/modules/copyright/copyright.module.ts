import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CopyrightRoutingModule } from './copyright-routing.module';
import { CopyrightComponent } from './copyright.component';


@NgModule({
  declarations: [CopyrightComponent],
  imports: [
    CommonModule,
    CopyrightRoutingModule
  ]
})
export class CopyrightModule { }
