import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [PortfolioComponent],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    FontAwesomeModule,
    NgSlimScrollModule,
    SharedModule
  ],
  providers: [{
    provide: SLIMSCROLL_DEFAULTS,
    useValue: {
      alwaysVisible: false,
      gridOpacity: '0.2', barOpacity: '0.5',
      gridBackground: '#fff',
      gridWidth: '6',
      gridMargin: '2px 2px',
      barBackground: '#fff',
      barWidth: '20',
      barMargin: '2px 2px'
    }
  }],
})
export class PortfolioModule { }
