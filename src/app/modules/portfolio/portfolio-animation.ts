import { trigger, state, style, animate, transition, query, animateChild, group } from '@angular/animations';

export const fadeInAnimation =
  trigger('fadeIn', [
    transition(':enter', [
      style({ bottom: '-20px', opacity: '0' }),
      animate('.5s ease-out', style({ bottom: '0px', opacity: '1' })),
    ]),
    transition(':leave', [
      style({ bottom: '0px', opacity: '1' }),
      animate('.5s ease-out', style({ bottom: '0px', opacity: '0' })),
    ]),
  ]);
