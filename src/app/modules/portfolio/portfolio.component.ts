import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  HostBinding
} from '@angular/core';
import { faImage, faCameraRetro, faFilm } from '@fortawesome/free-solid-svg-icons';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { fadeInAnimation } from './portfolio-animation';

import * as $ from 'jquery';
import 'magnific-popup';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
  animations: [
    fadeInAnimation,
  ]
})
export class PortfolioComponent implements OnInit, AfterViewInit {

  // it was in order to be able to apply style on router children
  @HostBinding('class') class = 'full-space-container';

  // used for the magnific popup library
  // https://dimsemenov.com/plugins/magnific-popup/documentation.html
  // https://github.com/dimsemenov/Magnific-Popup/blob/master/website/documentation.md
  @ViewChild('portfolioContent', {static: false}) imgArray: ElementRef;

  selected: string;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  faImage = faImage;
  faCameraRetro = faCameraRetro;
  faFilm = faFilm;

  items = [
    {title: 'Glenn Canyon', type: 'photo', url: 'assets/images/photo1.jpeg',
      fallback: '', icon: faCameraRetro, luminosity: 'dark'},
    {title: 'Grand Canyon', type: 'photo', url: 'assets/images/photo3.jpeg',
      fallback: '', icon: faCameraRetro, luminosity: 'dark'},
    // {title: 'Sunset Grand Canyon', type: 'photo', url: 'assets/images/photo2.jpeg',
    //   fallback: '', icon: faCameraRetro, luminosity: 'dark'},
    // {title: 'Monument Valley', type: 'photo', url: 'assets/images/photo4.jpeg',
    //   fallback: '', icon: faCameraRetro, luminosity: 'dark'},
    {title: 'Bryce Canyon', type: 'video', url: 'assets/videos/video1.mp4',
      fallback: 'assets/images/fallback-video1.PNG', icon: faFilm, luminosity: 'dark'},
    // {title: 'Grand Canyon overview', type: 'photo', url: 'assets/images/photo5.jpeg',
    //   fallback: '', icon: faCameraRetro, luminosity: 'dark'},
    {title: 'Notification center', type: 'design', url: 'assets/images/design-notification-center-1.png',
      fallback: '', icon: faImage, luminosity: 'light'},
    {title: 'Login page', type: 'design', url: 'assets/images/design-login-page-1.png',
      fallback: '', icon: faImage, luminosity: 'light'},
    {title: 'Login page 2', type: 'design', url: 'assets/images/design-login-page-2.png',
      fallback: '', icon: faImage, luminosity: 'light'},
    {title: 'Profile page', type: 'design', url: 'assets/images/design-profile-page-1.png',
      fallback: '', icon: faImage, luminosity: 'light'}
  ];

  filters = [
    {title: 'All'},
    {title: 'Photo'},
    {title: 'Video'},
    {title: 'Design'}
  ];

  constructor(private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.selected = this.filters[0].title;
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right', // left | right
      barBackground: 'grey', // #C9C9C9
      barOpacity: '0.6', // 0.8
      barWidth: '6', // 10
      barBorderRadius: '70', // 20
      barMargin: '0', // 0
      gridBackground: '#d9d9d9', // #D9D9D9
      gridOpacity: '0', // 1
      gridWidth: '2', // 2
      gridBorderRadius: '20', // 20
      gridMargin: '0', // 0
      alwaysVisible: false, // true
      visibleTimeout: 1000, // 1000
    };
  }

  ngAfterViewInit(): void { // this is use to make the magnific popup work on the images
    this.addMagnificPopupToItems();
  }

  addMagnificPopupToItems() {
    // console.log(this.imgArray.nativeElement.children);
    const htmlCollection = this.imgArray.nativeElement.children;
    for (let i = 0; i < htmlCollection.length; i++) {
      $(htmlCollection[i]).magnificPopup({
        items: [
          {
            src: this.items[i].url,
            type: this.items[i].type === 'photo' || this.items[i].type === 'design' ? 'image' : 'iframe' // this overrides default type
          }
        ],
        gallery: {
          enabled: false
        },
        type: 'image' // this is a default type
      });
    }
  }

  select(filter) {
    // Everytime time we filter, we put back in the selection all the items first,
    // this is needed for the magnific popup feature
    this.selected = 'All';
    // next 2 calls are needed to make the magnific popup come back
    // after that objects that have been filtered out reappears
    this.changeDetector.detectChanges();
    this.addMagnificPopupToItems();
    this.selected = filter.title;
    // next 2 calls are needed to make the magnific popup come back
    // after that objects that have been filtered out reappears
    this.changeDetector.detectChanges();
    this.addMagnificPopupToItems();
  }

  isActive(filter) {
    return this.selected === filter.title;
  }

}
