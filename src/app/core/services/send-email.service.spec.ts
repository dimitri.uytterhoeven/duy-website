import { TestBed } from '@angular/core/testing';

import { SendEmailService } from './send-email.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('SendEmailService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ HttpClient, HttpHandler ]
  }));

  it('should be created', () => {
    const service: SendEmailService = TestBed.get(SendEmailService);
    expect(service).toBeTruthy();
  });
});
