import { TestBed } from '@angular/core/testing';

import { FileDownloadService } from './file-download.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('FileDownloadService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ HttpClient, HttpHandler ]
  }));

  it('should be created', () => {
    const service: FileDownloadService = TestBed.get(FileDownloadService);
    expect(service).toBeTruthy();
  });
});
