import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FileDownloadService {

  constructor(private httpClient: HttpClient) {}

  downloadFile(url): Observable<Blob> {
    return this.httpClient.get(url, { responseType: 'blob'});
  }

}
