import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from '../models/Contact';

@Injectable({
  providedIn: 'root'
})
export class SendEmailService {

  baseUrl = 'http://localhost:3030/api/send-email';

  constructor(private httpClient: HttpClient) {}

  sendEmail(contact: Contact): Observable<any> {
    const body = {
      body: contact.message,
      from: contact.email,
      firstName: contact.firstname,
      lastName: contact.lastname
    };
    return this.httpClient.post(this.baseUrl, body, {
      headers: new HttpHeaders( {
        'Content-Type': 'application/json',
      })
    });
  }
}
