import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { ProfileModule } from './modules/profile/profile.module';
import { AsideModule } from './modules/aside/aside.module';
import { MenuModule } from './modules/menu/menu.module';
import { ParticlesModule } from 'angular-particle';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        FontAwesomeModule,
        AppRoutingModule,
        ProfileModule,
        AsideModule,
        MenuModule,
        ParticlesModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
          progressBar: true,
          tapToDismiss: true,
          newestOnTop: true,
          maxOpened: 5,
          timeOut: 3000,
          positionClass: 'toast-top-center',
          preventDuplicates: false,
        }) // ToastrModule added
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'duy-website'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('duy-website');
  });
});
