import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contentFilter',
  pure: false
})
export class ContentFilterPipe implements PipeTransform {
  transform(items: any[], filter: any): any {
    if (!items || !filter) {
      return items;
    }

    if (filter.title.toLowerCase() === 'all') {
      return items;
    }

    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter(item => item.type.indexOf(filter.title.toLowerCase()) !== -1);
  }
}
