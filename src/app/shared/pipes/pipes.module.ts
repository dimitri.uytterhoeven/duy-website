import { NgModule } from '@angular/core';

import { ContentFilterPipe } from './contentFilter.pipe';

@NgModule({
  declarations: [
    ContentFilterPipe,
  ],
  imports: [],
  exports: [
    ContentFilterPipe,
  ]
})
export class PipesModule {}
